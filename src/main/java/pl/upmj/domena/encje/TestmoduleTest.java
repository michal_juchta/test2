package pl.upmj.domena.encje;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "testmodule_test")
@Inheritance(strategy = InheritanceType.JOINED)
@Getter @Setter
public class TestmoduleTest {

    @Id
    @Column(name = "id_testmodule_test", nullable = false)
    private Integer idTestmoduleTest;

    @Column(name = "test_string", length = 500)
    private String testString;

    public void setTestString(String testString) {
        this.testString = testString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TestmoduleTest that = (TestmoduleTest) o;

        if (!Objects.equals(idTestmoduleTest, that.idTestmoduleTest))
            return false;
        if (!Objects.equals(testString, that.testString)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = idTestmoduleTest != null ? idTestmoduleTest.hashCode() : 0;
        result = 31 * result + (testString != null ? testString.hashCode() : 0);
        return result;
    }
}
