package pl.upmj.domena.encje;

import pl.upmj.domena.rest.TestmoduleTestLangDto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "testmodule_test_lang")
public class TestmoduleTestLang extends TestmoduleTest{

    @Column(name = "id_testmodule_test_lang", nullable = false)
    private Integer idTestmoduleTestLang;

    @Column(name = "id_lang", nullable = false)
    private Integer idLang;

    @Column(name = "id_shop", nullable = false)
    private Integer idShop;

    @Column(name = "test_lang", length = 500)
    private String testLang;


    public TestmoduleTestLangDto toDTO() {
        return new TestmoduleTestLangDto(getIdTestmoduleTest(), getTestString(), idTestmoduleTestLang, idLang, idShop, testLang);
    }
}
