package pl.upmj.domena.repozytoria;

import org.hibernate.Session;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.lang.reflect.ParameterizedType;
import java.util.List;

public abstract class AbstractHibernateRepository<ENTITY, KEY> implements HibernateRepository<ENTITY, KEY> {

    @PersistenceContext
    protected EntityManager em;

    protected Class clazz;

    public AbstractHibernateRepository() {

        ParameterizedType parametrizedType = null;

        if (getClass().getGenericSuperclass() instanceof ParameterizedType) {// class
            parametrizedType = (ParameterizedType) getClass().getGenericSuperclass();
        } else if (getClass().getGenericSuperclass() instanceof Class) {// in case of CGLIB proxy
            parametrizedType = (ParameterizedType) ((Class) getClass().getGenericSuperclass()).getGenericSuperclass();
        } else {
            throw new IllegalStateException("GenericDAOImpl - class " + getClass() + " is not subtype of ParametrizedType.");
        }

        this.clazz = ((Class<ENTITY>) parametrizedType.getActualTypeArguments()[0]);
    }

    @Override
    public ENTITY find(KEY id) {
        return (ENTITY) em.find(clazz, id);
    }

    @Override
    public KEY save(final ENTITY entity) {
        return (KEY) getSession().save(entity);
    }

    @Override
    public void delete(KEY id) {
        ENTITY e = (ENTITY) em.find(clazz, id);
        em.remove(e);
    }

    @Override
    public ENTITY merge(ENTITY entity) {
        return em.merge(entity);
    }

    public Session getSession() {
        return (Session) em.getDelegate();
    }

    @Override
    public List<ENTITY> findAll() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<ENTITY> cq = cb.createQuery(clazz);
        Root<ENTITY> rootEntry = cq.from(clazz);
        CriteriaQuery<ENTITY> all = cq.select(rootEntry);
        TypedQuery<ENTITY> allQuery = em.createQuery(all);
        return allQuery.getResultList();
    }

}
