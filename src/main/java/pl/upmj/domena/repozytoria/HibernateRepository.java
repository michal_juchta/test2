package pl.upmj.domena.repozytoria;

import java.util.List;

public interface HibernateRepository<ENTITY, KEY> {

    ENTITY find(KEY id);

    KEY save(final ENTITY entity);

    ENTITY merge(final ENTITY entity);

    void delete(final KEY id);

    List<ENTITY> findAll();

}
