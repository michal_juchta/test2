package pl.upmj.domena.rest;

import lombok.Data;

@Data
public class TestmoduleTestLangDto {

    private Integer idTestmoduleTest;

    private String testString;

    private Integer idTestmoduleTestLang;

    private Integer idLang;

    private Integer idShop;

    private String testLang;

    public TestmoduleTestLangDto(Integer idTestmoduleTest, String testString, Integer idTestmoduleTestLang, Integer idLang, Integer idShop, String testLang) {
        this.idTestmoduleTest = idTestmoduleTest;
        this.testString = testString;
        this.idTestmoduleTestLang = idTestmoduleTestLang;
        this.idLang = idLang;
        this.idShop = idShop;
        this.testLang = testLang;
    }
}
