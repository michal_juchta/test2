package pl.upmj.domena.rest;

import com.google.common.collect.Lists;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import pl.upmj.domena.encje.TestmoduleTest;
import pl.upmj.domena.encje.TestmoduleTestLang;
import pl.upmj.domena.repozytoria.TestLangRepository;
import pl.upmj.domena.repozytoria.TestRepository;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
@RestController
@RequestMapping("/test")
public class TestController {

    @Inject
    private TestRepository testRepository;
    @Inject
    private TestLangRepository testLangRepository;

    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest req, Exception ex) {
        ModelAndView mav = new ModelAndView();
        mav.addObject("exception", ex);
        mav.addObject("url", req.getRequestURL());
        mav.setViewName("error");
        return mav;
    }

    /**
     * @return List<PracownikDTO>
     * @Example: http://localhost:8080/test
     */
    @GetMapping("/1")
    public ResponseEntity<TestmoduleTest> getLista1() {
        List<TestmoduleTest> list = testRepository.findAll();
        return new ResponseEntity(list, HttpStatus.OK);
    }

    @GetMapping("/2")
    public ResponseEntity<TestmoduleTestLang> getLista2() {
        List<TestmoduleTestLang> list = testLangRepository.findAll();
        return new ResponseEntity(list, HttpStatus.OK);
    }

    @GetMapping("/3")
    public ResponseEntity<TestmoduleTestLangDto> getLista3() {
        List<TestmoduleTestLang> list = testLangRepository.findAll();
        List<TestmoduleTestLangDto> dtoList = Lists.newLinkedList();
        for (TestmoduleTestLang testmoduleTestLang : list) {
            dtoList.add(testmoduleTestLang.toDTO());
        }
        return new ResponseEntity(dtoList, HttpStatus.OK);
    }

}
