package pl.upmj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MsoManager {

    public static void main(String[] args) {
        SpringApplication.run(MsoManager.class, args);
    }

}
